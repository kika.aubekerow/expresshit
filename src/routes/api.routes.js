const express = require('express');

const TasksService = require('../services/tasksService');

const router = express.Router();

module.exports = () => {
  router.post('/tasks/:taskName/done', async (req, res) => {
    const task = req.params.taskName;
    await (new TasksService().doneTaks(task));

    return res.redirect('/');
  });

  return router;
};
