const express = require('express');

const { check } = require('express-validator');

const TasksService = require('../services/tasksService');
const apiRouter = require('./api.routes');

const router = express.Router();

const { getMainPage, postNewTask } = require('../controllers/index');

const taskValidator = [
  check('task')
    .trim()
    .isLength({ min: 3 })
    .escape()
    .withMessage('A task is required'),
];

module.exports = () => {
  router.get('/', getMainPage);

  router.use('/api', apiRouter());

  router.get('/done', async (req, res) => {
    const done = await (new TasksService().getDone());
    return res.render('layout', { template: 'done', doneTasks: done });
  });

  router.post(
    '/',
    taskValidator,
    postNewTask,
  );

  router.get('/done', async (req, res) => {
    const done = await (new TasksService().getDone());
    return res.render('layout', { template: 'done', doneTasks: done });
  });

  return router;
};
