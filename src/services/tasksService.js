/* eslint-disable class-methods-use-this */
const DONE = [];
let ACTIVE = [];

class TasksService {
  async getDone() {
    return DONE;
  }

  async getActive() {
    return ACTIVE;
  }

  async addActive(taskName) {
    ACTIVE.push(taskName);
  }

  async removeFromActive(taskName) {
    const taskIndex = ACTIVE.indexOf(taskName);
    if (taskIndex !== -1) {
      delete ACTIVE[taskIndex];
      ACTIVE = ACTIVE.filter((n) => n);
    }
  }

  async doneTaks(taskName) {
    await this.removeFromActive(taskName);
    DONE.push(taskName);
  }
}

module.exports = TasksService;
