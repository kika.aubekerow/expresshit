const { validationResult } = require('express-validator');

const TasksService = require('../services/tasksService');

const getMainPage = async (req, res) => {
  const active = await (new TasksService().getActive());

  const isRedirectFromPost = Array.isArray(req.session.errors);

  if (isRedirectFromPost) {
    const success = req.session.errors.length === 0;
    const { errors } = req.session;
    req.session.errors = undefined;

    return res.render('layout', {
      template: 'index', tasks: active, success, errors,
    });
  }

  return res.render('layout', { template: 'index', tasks: active, success: undefined });
};

const postNewTask = async (req, res) => {
  const errors = validationResult(req);
  req.session.errors = errors.array();

  if (!errors.isEmpty()) {
    return res.redirect('/');
  }

  const { task } = req.body;

  await new TasksService().addActive(task);

  return res.redirect('/');
};

exports.getMainPage = getMainPage;
exports.postNewTask = postNewTask;
