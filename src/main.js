const express = require('express');
const makeStoppable = require('stoppable');
const http = require('http');
const path = require('path');
const cookieSession = require('cookie-session');
const bodyParser = require('body-parser');

const app = express();
const router = require('./routes/index');

app.use(cookieSession({
  name: 'session',
  keys: ['dededde', 'ecdc'],
}));

const server = makeStoppable(http.createServer(app));

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, './views'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, '../assets')));

app.use(router());

module.exports = () => {
  const stopServer = () => new Promise((resolve) => {
    server.stop(resolve);
  });

  return new Promise((resolve) => {
    server.listen(3000, () => {
      console.log('Express server is listening on http://localhost:3000');
      resolve(stopServer);
    });
  });
};
